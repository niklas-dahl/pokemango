FROM alpine

ADD server /server
WORKDIR /server
RUN chmod +x start.sh

RUN apk add --update git python python-dev py-pip build-base bash
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ['/server/start.sh']